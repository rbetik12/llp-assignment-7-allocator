CC=gcc
CFLAGS=-std=gnu99 -Iinclude -g

all: lab7

lab7: main.o mem.o
	$(CC) $(CFLAGS) main.o mem.o -o lab7

main.o:
	$(CC) $(CFLAGS) -c src/main.c

mem.o:
	$(CC) $(CFLAGS) -c src/mem.c

clean:
	rm -rf main.o mem.o lab7