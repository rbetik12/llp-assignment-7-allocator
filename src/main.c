#include <stdio.h>
#include <sys/mman.h>
#include <signal.h>
#include "mem.h"

int main() {
    heap_init(PAGE_SIZE);
    puts("=====================Start====================="); memalloc_debug_heap(stdout);

    char *str = _malloc(5000);
    puts("=====================Allocated first block=====================");
    memalloc_debug_heap(stdout);
    char *str2 = _malloc(228);
    puts("=====================Allocated second block=====================");
    memalloc_debug_heap(stdout);
    char *str3 = _malloc(35605);
    puts("=====================Allocated third block=====================");
    memalloc_debug_heap(stdout);

    scanf("%s", str);
    printf("Your string is: %s\n", str);
    puts("=====================Memory state after writing to it=====================");
    memalloc_debug_heap(stdout);

    _free(str);
    puts("=====================Freed first block=====================");
    memalloc_debug_heap(stdout);
    _free(str2);
    puts("=====================Freed second block=====================");
    memalloc_debug_heap(stdout);
    _free(str3);
    puts("=====================Freed third block=====================");
    memalloc_debug_heap(stdout);

    int* leak_test = _malloc(40 * sizeof(int));
    puts("=====================Leak test=====================");
    memalloc_debug_heap(stdout);
    check_for_leak();
    _free(leak_test);
    memalloc_debug_heap(stdout);
    check_for_leak();
    return 0;
}
