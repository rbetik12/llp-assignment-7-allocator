#include "mem.h"
#include <stdint.h>
#include <sys/mman.h>
#include <signal.h>
#include <fcntl.h>
#include <stdlib.h>
#include <math.h>

struct last_pointer {
    struct mem* pointer;
    size_t size;
};

static struct mem* heap_start = NULL;
static bool is_pointer_okay = true;
static struct last_pointer *last_pointer;
static size_t faultedPage = 0;
static size_t total_mem_allocated = 0;

void * create_page_in_file(void* address, uint64_t size) {
    if (size >= 20 * pow(10, 9)) {
        return NULL;
    }
    char filename[30];
    sprintf(filename, "%zu", faultedPage);
    int file = open(filename, O_RDWR | O_CREAT | O_TRUNC, (mode_t) 0600);
    faultedPage += 1;
    if (file == -1) {
        perror("Can't open file!");
        exit(EXIT_FAILURE);
    }
    return mmap(address, size,
                PROT_WRITE | PROT_READ,
                MAP_PRIVATE,
                file, 0
    );
}

void* create_page(void* address, uint64_t size, bool fixed) {
    total_mem_allocated += size;
    if (total_mem_allocated >= 16 * pow(10, 9)) {
        return create_page_in_file(address, size);
    }
    return mmap(
            address,
            size,
            PROT_READ | PROT_WRITE,
            MAP_PRIVATE | MAP_ANONYMOUS | (fixed ? MAP_FIXED_NOREPLACE : 0),
            -1,
            0
    );

}

void reallocate_faulted_page() {
    printf("Last pointer size: %zu\n", last_pointer->size);
    last_pointer->pointer = create_page_in_file(HEAP_START, last_pointer->size);
    if (last_pointer->pointer == NULL) {
        perror("Can't allocated such large chunk of memory!\n");
        exit(EXIT_FAILURE);
    }
    is_pointer_okay = true;
}

void sig_handler(int signo) {
    if (signo == SIGSEGV) {
        is_pointer_okay = false;
        printf("Page fault!\n");
        reallocate_faulted_page();
    }
}

void* heap_init(size_t initial_size) {
    last_pointer = create_page(NULL, sizeof(struct last_pointer), false);
    signal(SIGSEGV, sig_handler);
    if (initial_size < PAGE_SIZE) initial_size = PAGE_SIZE;
    heap_start = create_page(HEAP_START, initial_size, false);
    last_pointer->pointer = heap_start;
    last_pointer->size = initial_size - sizeof(struct mem);
    printf("Last pointer size: %zu\n", last_pointer->size);
    heap_start->capacity = initial_size - sizeof(struct mem);
    heap_start->is_free = true;
    heap_start->next = NULL;
    printf("Initialized heap at %p, size %lu\n", (void*) heap_start, initial_size);
    return heap_start;
}

void split_block(struct mem* block, size_t query) {
    struct mem* new_block = (struct mem*) ((char*) block + query + sizeof(struct mem));
    new_block->is_free = true;
    new_block->next = block->next;
    new_block->capacity = block->capacity - query - sizeof(struct mem);

    block->next = new_block;
    block->capacity = query;
}

bool is_block_splitable(struct mem* block, size_t query) {
    return block->capacity >= query + sizeof(struct mem) + BLOCK_MIN_SIZE;
}

void* _malloc(size_t query) {
    if (!heap_start) heap_init(PAGE_SIZE);
    if (query <= 0) return NULL;
    if (query < BLOCK_MIN_SIZE) query = BLOCK_MIN_SIZE;

    struct mem* blockPointer = heap_start;
    struct mem* previousBlock = NULL;

    while (blockPointer != NULL) {
        previousBlock = blockPointer;
        if (!blockPointer->is_free) {
            blockPointer = blockPointer->next;
            continue;
        }

        if (blockPointer->capacity != query) {
            if (!is_block_splitable(blockPointer, query)) {
                blockPointer = blockPointer->next;
                continue;
            }
            split_block(blockPointer, query);
        }

        blockPointer->is_free = false;
        last_pointer->pointer = blockPointer;
        last_pointer->size = blockPointer->capacity + sizeof(struct mem);
        return (char*) blockPointer + sizeof(struct mem);
    }

    void* last_block_end = previousBlock->capacity + sizeof(struct mem) + (char*) previousBlock;
    size_t page_size = query < PAGE_SIZE ? PAGE_SIZE : query;
    struct mem* new_page = create_page(last_block_end, page_size, true);

    if (new_page == MAP_FAILED) {
        new_page = create_page(last_block_end, page_size, false);
        if (new_page == MAP_FAILED)
            return NULL;
    }

    previousBlock->next = new_page;
    new_page->capacity = page_size - sizeof(struct mem);
    new_page->is_free = false;
    new_page->next = NULL;

    if (is_block_splitable(new_page, query)) split_block(new_page, query);

    last_pointer->pointer = new_page;
    last_pointer->size = new_page->capacity + sizeof(struct mem);
    return (char*) new_page + sizeof(struct mem);
}

void merge_blocks(struct mem* leftBlock, struct mem* rightBlock) {
    leftBlock->capacity += rightBlock->capacity + sizeof(struct mem);
    leftBlock->next = rightBlock->next;
}

void check_for_adjacent_blocks(struct mem* prevBlock, struct mem* block) {
    if (prevBlock == NULL || block == NULL) return;
    if (block->next && block->next->is_free) {
        merge_blocks(block, block->next);
    }
    if (prevBlock && prevBlock->is_free) {
        merge_blocks(prevBlock, block);
    }
}

void _free(void* mem) {
    struct mem *block = (struct mem*) ((char*) mem - sizeof(struct mem));

    if (!block) return;

    struct mem* curBlockPointer = block;
    struct mem* prevBlockPointer = NULL;
    bool blockFound = false;
    while (curBlockPointer != NULL) {
        if (curBlockPointer == block) {
            blockFound = true;
            break;
        }
        prevBlockPointer = curBlockPointer;
        curBlockPointer = curBlockPointer->next;
    }

    if (!blockFound) return;

    block->is_free = true;
    gc(heap_start);
}

void check_for_leak() {
    struct mem* curBlockPointer = heap_start;
    bool has_leak = false;
    while (curBlockPointer != NULL) {
        if (!curBlockPointer->is_free) {
            fprintf(stderr, "Warning: current block at address %p and size of %zu bytes is not free. Could be potential memory leak!\n",
                    (void*)curBlockPointer, curBlockPointer->capacity);
            has_leak = true;
        }
        curBlockPointer = curBlockPointer->next;
    }

    if (!has_leak) {
        fprintf(stderr, "No leaks is present!\n");
    }
}

void gc() {
    struct mem* curBlockPointer = heap_start;
    struct mem* prevBlockPointer = NULL;
    while (curBlockPointer != NULL) {
        prevBlockPointer = curBlockPointer;
        curBlockPointer = curBlockPointer->next;
        check_for_adjacent_blocks(prevBlockPointer, curBlockPointer);
    }
}

void memalloc_debug_heap(FILE* f) {
    struct mem* ptr = heap_start;
    for (; ptr; ptr = ptr->next)
        memalloc_debug_struct_info(f, ptr);
}

void memalloc_debug_struct_info(FILE* f, struct mem const* const address) {
    uint64_t i;
    char* ptr = (char*) address + sizeof(struct mem);

    fprintf(f, "start: %p, size: %4lu, is_free: %d, bytes:", (void*) address, address->capacity, address->is_free);
    for (i = 0; i < DEBUG_FIRST_BYTES && i < address->capacity; i++, ptr++) {
        fprintf(f, " %2hhX", *ptr);
    }
    putc('\n', f);
}


